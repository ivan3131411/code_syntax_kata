class SyntaxValidation

  CORRECT_CHARS = "< >"
  GOOD_VALIDATION = true

  def correct_characters?(code)
    if !valid_chars?(code) || code[-1] == '<'
      return false
    end

    GOOD_VALIDATION
  end

  def valid_chars?(code)
    code.each_char do |char|
      return false if !CORRECT_CHARS.include?(char)
    end

    GOOD_VALIDATION
  end

end