require_relative 'validate_characters'

class Linter
  class << self

    GOOD_VALIDATION = true

    def validate_code(code)
      caracter_validation = SyntaxValidation.new.correct_characters?(code)
      order_validation = correct_order?(code)

      if (!caracter_validation || !order_validation)
        return false

      else
        return GOOD_VALIDATION
      end
    end

    def correct_order?(code)
      code = code.split('')

      while !code.length.zero?

        for i in 0..code.length do
          return false if code[0] == '>'

          code.delete_at(i)

          for a in 0..code.length do
            if code[a] == '>'
              code.delete_at(a)
              break
            end
          end
        end
      end

      GOOD_VALIDATION
    end

  end
end
