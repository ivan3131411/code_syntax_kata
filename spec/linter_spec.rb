require_relative '../src/linter'

describe 'Linter' do

  it "only gets '<' and '>' as string'" do
    recived_code = '<>'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(true)
  end

  it "the starter character is '<' " do
    recived_code = '<>'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(true)
  end

  it "dosnt pass the check if the starter character is '>' " do
    recived_code = '><>'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(false)
  end

  it "dosnt pass the check if the lastest character is '<' " do
    recived_code = '<><'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(false)
  end

  it "checks that every '<' closes with a '>'" do
    recived_code = '<><><>'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(true)
  end

  it "dont pass check with complex codes like <>><<>" do
    recived_code = '<>><<>'

    correct_grammar = Linter.validate_code(recived_code)

    expect(correct_grammar).to eq(false)
  end

end
