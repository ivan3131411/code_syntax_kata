1 test - 'only gets '<' and '>' as string' el programa se asegura de que solo recibe < o > como argumentos

2 test - 'pass the check if the starter character is '<' ' el programa comprobara que el primer caracter sea el simbolo <
  y pase el test

3 test - 'dosn't pass the check if the starter character is '>' ' el programa comprobara que el primer caracter sea
  el simbolo > y no pase el check

4 test - 'dosn't pass the check if the lastest character is '<' ' el programa comprobara que el ultimo caracter sea
  el simbolo < y no pasara el check

5 test - 'checks that every '<' closes with a '>' ' el programa comprobara que cada simbolo de apertura
 tiene un simbolo de cierre

 6 test - 'dont pass check with complex codes like <>><<>' el programa comprobara que cada simbolo de apertura
 tiene un simbolo de cierre
